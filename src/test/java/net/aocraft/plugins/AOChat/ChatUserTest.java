package net.aocraft.plugins.AOChat;

import junit.framework.TestCase;

public class ChatUserTest extends TestCase {
	private ChatUser user1;
	private ChatUser user2;
	private String username1;
	private String username2;
	private String channelname1;
	private viewPort viewport1;
	private viewPort viewport2;
	
	
	
	private void setup() {
		// setup channel
		username1 = "User 1";
		username2 = "User 2";
		channelname1 = "General";
		user1 = new ChatUser(username1);
		user1 = new ChatUser(username1);
		viewport1 = new viewPort(5, 5, 205, 805, 12);
		viewport2 = new viewPort(5, 5, 805, 1005, 12);
	}

	public void testChatUser() {
		ChatUser user1 = new ChatUser(username1);
		assertNotNull(user1);
	    user1.setChUserName(username1);
	    user1.setChLastUsed(channelname1);
	    user1.addViewPort(viewport1);
	    
		assertEquals(username1, user1.getChUserName());
		assertEquals(channelname1, user1.getChLastUsed());
		assertEquals(viewport1, user1.getViewPorts().get(0));
	}
	
	public void testAddViewPort() {
		setup();
		assertEquals(0, user1.chViewPorts.size());  		// User should have no ViewPort on initialization
		user1.addViewPort(viewport1);
		assertEquals(1, user1.chViewPorts.size());			// Number of ViewPorts should be 1 after adding a viewPort
		assertEquals(0,user1.chViewPorts.indexOf(viewport1));	// viewPort1 should exist be at top of array			

		user1.addViewPort(viewport2);
		assertEquals(2, user1.chViewPorts.size());			// Number of ViewPorts should be 2 after adding a viewPort
		assertEquals(1,user1.chViewPorts.indexOf(viewport2));	// viewPort2 should exist be at bottom of array			
		
	}
	
	public void testRemoveViewPort() {
		setup();
		assertEquals(0, user1.chViewPorts.size());  		// User should have no ViewPort on initialization
		user1.addViewPort(viewport1);
		user1.addViewPort(viewport2);
        user1.removeViewPort(viewport1);
        
		assertEquals(1, user1.chViewPorts.size());			// Number of ViewPorts should be 1 after removing a viewPort
		assertEquals(0,user1.chViewPorts.indexOf(viewport2));	// viewPort2 should exist be at top of array			
			
		
	}
}

package net.aocraft.plugins.AOChat;

import junit.framework.TestCase;

public class ChatChannelTest extends TestCase {

	private ChatChannel ch1;
	private String channelName;
	private String username1;
	private String username2;
	
	private void setup() {
		// setup channel
		channelName="General";
		String chType = "local";
		ch1 = new ChatChannel(channelName, chType);
		username1 = "TestUser1";
		username2 = "TestUser2";
			
	
		
	}

	public void testChatChannel() {
		String chType = "local";
		ChatChannel ch1 = new ChatChannel(channelName, chType);
		ch1.setChDescription("Default chat channel");
		ch1.setChPass("test");
		ch1.setChRange(100);
		assertEquals(channelName, ch1.getChName());
		assertEquals("local", ch1.getChType());
		assertEquals("Default chat channel", ch1.getChDescription());
		assertEquals("test", ch1.getChPass());
		assertEquals(0, ch1.chUsers.size());
			
	}
	
	public void testAddChannelUser() {
		// setup test values
		setup();
		
		// test method
		assertEquals(0, ch1.chUsers.size());				// Channel should have no users on initialization
		ch1.addChannelUser(username1);
		assertEquals(1, ch1.chUsers.size());				// Number of users should be 1 after adding a user
		assertEquals(0,ch1.chUsers.indexOf(username1));		// userName1 should exist be at top of array			
		ch1.addChannelUser(username2);					
		assertEquals(1,ch1.chUsers.indexOf(username2));		// userName1 should exist be at second position of array	
		assertEquals(2, ch1.chUsers.size());				// Number of users should be 2 after adding another user
		
	}
	
	public void testRemoveUser() {
		// Setup test values
		setup();
		// Test the method
		assertEquals(0, ch1.chUsers.size());				// Channel should have no users on initialization
		ch1.addChannelUser(username1);
		ch1.addChannelUser(username2);				
		assertEquals(2, ch1.chUsers.size());				// Number of users should be 2 by now
		ch1.removeChannelUser(username2);
		assertEquals(1, ch1.chUsers.size());				// Number of users should be 1 after remove
		assertEquals(-1,ch1.chUsers.indexOf(username2));	// userName2 should not exist in the array anymore	
		assertEquals(0,ch1.chUsers.indexOf(username1));		// userName1 should exist be at top of array	
			
		
	}
}

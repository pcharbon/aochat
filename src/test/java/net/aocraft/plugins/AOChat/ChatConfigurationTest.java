package net.aocraft.plugins.AOChat;

import junit.framework.TestCase;

public class ChatConfigurationTest extends TestCase {
	
	/**
	 * Verify that the default values for the chatConfiguration are set 
	 * @result	ChatConfiguration defaults are set on initialization
	 */
	@SuppressWarnings("static-access")
	public void testDefaultValues() {
		
		ChatConfiguration config1 = new ChatConfiguration(null);			
		assertNotNull(config1);														// Object should be instantiated

		assertNotNull(config1.BROADCAST);											// Field BROADCAST should not be null						
		assertEquals("true", config1.BROADCAST.getDefaultValue().toString());		// Default value of BROADCAST configuration should be set to true
		
		assertNotNull(config1.USERCHANNELS);										// Field BROADCAST should not be null						
		assertEquals("true", config1.USERCHANNELS.getDefaultValue().toString());	// Default value of USERCHANNELS configuration should be set to true
		
		assertNotNull(config1.PRIVATEMESSAGES);										// Field PRIVATEMESSAGES should not be null						
		assertEquals("true", config1.PRIVATEMESSAGES.getDefaultValue().toString());	// Default value of PRIVATEMESSAGES configuration should be set to true

		assertNotNull(config1.OFFLINEMESSAGES);										// Field OFFLINEMESSAGES should not be null						
		assertEquals("true", config1.OFFLINEMESSAGES.getDefaultValue().toString());	// Default value of OFFLINEMESSAGES configuration should be set to true
		
		assertNotNull(config1.DEFAULTCHANNEL);											// Field DEFAULTCHANNEL should not be null						
		assertEquals("general", config1.DEFAULTCHANNEL.getDefaultValue().toString());	// Default value of DEFAULTCHANNEL configuration should be set to "general"
		
		assertNotNull(config1.CHANNELSFILE);											 // Field CHANNELSFILE should not be null						
		assertEquals("channels.yml", config1.CHANNELSFILE.getDefaultValue().toString()); // Default value of CHANNELSFILE configuration should be set to "channels.xml"
		
		assertNotNull(config1.LOCALCHAT);											// Field LOCALCHAT should not be null						
		assertEquals("true", config1.LOCALCHAT.getDefaultValue().toString());		// Default value of LOCALCHAT configuration should be set to true
	
	}

}

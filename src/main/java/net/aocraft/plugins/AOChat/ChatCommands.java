package net.aocraft.plugins.AOChat;

import net.aocraft.plugins.aosuite.AOSuite;
import java.util.ArrayList;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.spout.api.chat.ChatArguments;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.CommandPermissions;
import org.spout.api.command.annotated.Executor;
import org.spout.api.command.annotated.NestedCommand;
import org.spout.api.exception.CommandException;

public class ChatCommands {
	private final AOChat plugin;


	public ChatCommands(AOChat pPlugin) {
		this.plugin = pPlugin;
	}
	
		
	/**
	 * Version command will let a user display the currrent version of the Chat module
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"version"}, usage = "", desc = "Display the AO plugin Chat module version", min = 0, max = 0)
	public void version(CommandSource source, CommandContext args)  throws CommandException  {
		ChatArguments msg1 = new ChatArguments("AO Suite / AO Chat: " +AOChat.getChatInstance().getDescription().getVersion());
		ChatArguments msg2 = new ChatArguments("See http://www.aocraft.net for more information");
		AOChat.getChatMessageManager().Send(source.getName(),"system",msg1, msg1);
		AOChat.getChatMessageManager().Send(source.getName(),"system",msg2, msg2);
		
	}
	
		
	/**
	 * Join command will let a user join a specific chat channel
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"join"}, usage = "<chat channel> <password>", desc = "Join a chat channel", min = 1, max = 2)
	public void join(CommandContext args, CommandSource source) throws CommandException {
		if (args.length() == 1) {
			String userName = source.getName();
			String channelName = args.getString(0);
			if (AOChat.getChannelsManager().Exists(channelName)) {
				if (AOChat.getChannelsManager().getChannel(channelName).getChType()=="system") {
					throw new CommandException("The channel "+channelName+" is a reserved system channel.");

				}
				//AOChat.getAOInstance();
				AOChat.getUsersManager().getChatUser(userName).setChLastUsed(channelName);
				if (!AOChat.getChannelsManager().getChannel(channelName).exists(userName)) {
					AOChat.getChannelsManager().getChannel(channelName).addChannelUser(userName);
					// Add the newly joined channel to all the viewPorts by default
					// channelViews are currently generic until client-side phase
					AOChat.getUsersManager().getChatUser(userName).addChannelView(channelName,new ChannelView("Verdana","White",false, false));
					

					source.sendMessage("You have joined the "+channelName+" chat channel.");
					
				} else {
					source.sendMessage("You are now talking in "+channelName+".");
				}
			} else {
				throw new CommandException("The channel "+channelName+" does not exist.");
			}
			
		}
	}
	
	
	/**
	 * Leave command will let a user leave a specific chat channel
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"leave"}, usage = "<chat channel>", desc = "Leave a chat channel", min = 1, max = 1)
	public void leave(CommandContext args, CommandSource source) throws CommandException {
		if (args.length() == 1) {
			String userName = source.getName();
			String channelName = args.getString(0);
			if (AOChat.getChannelsManager().Exists(channelName)) {
				if (AOChat.getChannelsManager().getChannel(channelName).getChType()=="system") {
					throw new CommandException("The channel "+channelName+" is a reserved system channel.");

				}
				// TODO The default chat channel should be set in the plug-in configuration file
				AOChat.getUsersManager().getChatUser(userName).setChLastUsed("general");
				if (AOChat.getChannelsManager().getChannel(channelName).exists(userName)) {
					AOChat.getChannelsManager().getChannel(channelName).removeChannelUser(userName);
					// Remove the newly joined channel from all the viewPorts by default
					// channelViews are currently generic until client-side phase
					AOChat.getUsersManager().getChatUser(userName).removeChannelView(channelName);
					source.sendMessage("You have left the "+channelName+" chat channel.");
					
				}
			} else {
				throw new CommandException("You must specify which channel to leave!");
			}
			
		}
			
	}


	/**
	 * Channels command will let a user list all channels on server
	 * @param args
	 * @param source
	 * @throws CommandException
	 */
	@Command(aliases = {"channels"}, usage = "[<username>]", desc = "List all chat channels or the ones a user is registered to", min = 0, max = 1)
	public void channels(CommandContext args, CommandSource source) throws CommandException {

		source.sendMessage("Currently existing chat channels:");
		Collection<ChatChannel> channelList = AOChat.getChannelsManager().chatChannels.values();
		Integer number = 0;
		for (Iterator<?> iter = channelList.iterator(); iter.hasNext();) {
			ChatChannel channel = (ChatChannel) iter.next();
			number = number + 1;
			source.sendMessage(number.toString()+")  "+channel.chName+".");
		}
	}
	/**
	 * Channels command will let a user list all channels on server
	 * @param args
	 * @param source
	 * @throws CommandException
	 */

	@SuppressWarnings("deprecation")
	@Command(aliases = {"who","list"}, usage = "[<username>]", desc = "Display the information about one or more server players", min = 0, max = 1)
	public void who(CommandContext args, CommandSource source) throws CommandException {

		source.sendMessage("Current players list:");
		Collection<ChatUser> userList = AOChat.getUsersManager().chatUsers.values();
		for (Iterator<?> iter = userList.iterator(); iter.hasNext();) {
			ChatUser user = (ChatUser) iter.next();
			ChatArguments userInfo = new ChatArguments();
            userInfo.append(user.getChUserName());
            userInfo.append("\t");
//            userInfo.append(user.getUserStatus().toString());
//            userInfo.append("\t");
//			if (user.getUserStatus().toString()=="OFFLINE") {
//				Date last = user.getLastOnline();
//				userInfo.append(last.getMonth());
//				userInfo.append("/");
//				userInfo.append(last.getDay());
//				userInfo.append("/");
//				userInfo.append(last.getYear());
//	            userInfo.append("\t");
//				userInfo.append(last.getHours());
//				userInfo.append("h");
//				userInfo.append(last.getMinutes());
//			}
			source.sendMessage(userInfo);
		}
	}

	
}

package net.aocraft.plugins.AOChat;

import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.NestedCommand;
import org.spout.api.exception.CommandException;

public class ChatCommand {

	private final AOChat plugin;
	
	/**
	 * We must pass in an instance of our plugin's object for the annotation to register under the factory.
	 * @param instance
	 */
	public ChatCommand(AOChat instance) {
		plugin = instance;
	}
	
	/**
	 * Chat command is the module level command for all chat commands.
	 * The chat commands must follow the /ao chat <command> format
	 * and does nothing on its own
	 * 
	 * @throws CommandException
	 */
	@Command(aliases = {"chat"}, usage = "", desc = "Access AO Chat commands", min = 1, max = 1)
	//This is the class with all sub-commands under /ao chat 
	@NestedCommand(ChatCommands.class)
	public void chat(CommandContext args, CommandSource source) throws CommandException {
		//Chat command does nothing by itself, should list the sub-commands in a nice layout
	}

}

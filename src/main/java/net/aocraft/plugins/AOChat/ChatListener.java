/**
 * This class implements the Event Listener and check for Chat events
 */
package net.aocraft.plugins.AOChat;

import java.util.Date;



import org.spout.api.chat.ChatArguments;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerChatEvent;
import org.spout.api.event.player.PlayerLeaveEvent;
import org.spout.api.event.player.PlayerLoginEvent;
import org.spout.api.entity.*;

/**
 * @author Ducky
 *
 */
public class ChatListener implements Listener {
	
	private final AOChat plugin;
	
	public ChatListener(AOChat pPlugin) {
		this.plugin = pPlugin;
	}
	
	@EventHandler(order = Order.LATEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (!event.isAllowed()) {
			return;
		}
		Player player = event.getPlayer(); 
		String playerName = player.getName();
		ChatUser user;
		user = AOChat.getUsersManager().getChatUser(playerName);
		AOChat.getChatInstance().getLogger().info("Chat user "+playerName+" login in....");
		// Register the object listener 
		user.registerListener();
		// Join all preferred channels
		user.loadChannels();
	}

	@EventHandler(order = Order.LATEST)
	public void onPlayerLeave(PlayerLeaveEvent event) {

		Player player = event.getPlayer(); 
		String playerName = player.getName();
		ChatUser user;
		user = AOChat.getUsersManager().getChatUser(playerName);
		AOChat.getChatInstance().getLogger().info("Chat user "+playerName+" logging out....");
	
		// Save all current channels
		user.saveChannels();
		

	}

}

package net.aocraft.plugins.AOChat;

import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.NestedCommand;
import org.spout.api.exception.CommandException;

public class AoCommand {
	
	private final AOChat plugin;

	/**
	 * We must pass in an instance of our plugin's object for the annotation to register under the factory.
	 * @param instance
	 */
	public AoCommand(AOChat instance) {
		plugin = instance;
	}

	//This is the command. Will detail all the options later.
	@Command(aliases = {"ao"}, usage = "", desc = "Access AO commands", min = 1, max = 1)
	//This is the class with all sub-commands under /ao
	@NestedCommand(ChatCommand.class)
	public void ao(CommandContext args, CommandSource source) throws CommandException {
		//AO does nothing
	}
}

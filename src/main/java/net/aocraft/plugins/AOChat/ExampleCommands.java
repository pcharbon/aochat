package net.aocraft.plugins.AOChat;

import org.spout.api.Engine;
import org.spout.api.chat.style.ChatStyle;
import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.Executor;
import org.spout.api.exception.CommandException;
import org.spout.api.plugin.Platform;

public class ExampleCommands {

	public ExampleCommands() {
	}

	@Command(aliases = "kick", usage = "<name> [reason...]", desc = "Kick a player", min = 1, max = -1)
	private class KickCommand {
		@Executor(Platform.SERVER)
		public void handleServer(CommandSource source, CommandContext args) throws CommandException {
			source.sendMessage(ChatStyle.BLUE, "This is being executed on the server.");
		}

		@Executor(Platform.CLIENT)
		public void handleClient(CommandSource source, CommandContext args) throws CommandException {
			source.sendMessage(ChatStyle.BLUE, "This is being executed on the client.");
		}
	}

	@Command(aliases = "echo", usage = "<message...>", desc = "Echo a message", min = 1, max = -1)
	private class EchoCommand {
		@Executor(Platform.SERVER)
		public void handleServer(CommandSource source, CommandContext args) throws CommandException {
			source.sendMessage(ChatStyle.RED, "This is being executed on the server.");
			source.sendMessage(args.getJoinedString(0));
		}

		@Executor(Platform.CLIENT)
		public void handleClient(CommandSource source, CommandContext args) throws CommandException {
			source.sendMessage(ChatStyle.RED, "This is being executed on the client.");
			source.sendMessage(args.getJoinedString(0));
		}
	}

	@Command(aliases = "children", desc = "A command with children")
	private class ChildrenCommand {
		@Command(aliases = "first", desc = "The first child command", min = 0)
		private class FirstCommand {
			@Executor
			public void handle(CommandSource source, CommandContext args) {
				source.sendMessage("This is the first subcommand");
			}
		}

		@Command(aliases = "second", desc = "The second child command", min = 0)
		private class SecondCommand {
			@Executor
			public void handle(CommandSource source, CommandContext args) {
				source.sendMessage("This is the second subcommand");
			}
		}
	}
}



package net.aocraft.plugins.AOChat;

import java.io.File;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.yaml.YamlConfiguration;

public class UserConfiguration  extends ConfigurationHolderConfiguration {
	
	   // General
		public static final ConfigurationHolder NAME = new ConfigurationHolder("", "name");					// User name
		public static final ConfigurationHolder STATUS = new ConfigurationHolder("ONLINE", "status");		// User status (Online,Off-line,AFK,Muted,etc)
		public static final ConfigurationHolder LASTLOGIN = new ConfigurationHolder("", "lastlogin");		// Last time the user logged in
		public static final ConfigurationHolder LASTCHANNEL = new ConfigurationHolder("", "lastchannel");	// Last channel the user talked in
		
		
		
		public UserConfiguration(File dataFolder) {
	        super(new YamlConfiguration(new File(dataFolder, "config.yml")));
	    }
		
		@Override
		public void load() throws ConfigurationException {
			super.load();
			super.save();
		}

		@Override
		public void save() throws ConfigurationException {
			super.save();
		}	

}

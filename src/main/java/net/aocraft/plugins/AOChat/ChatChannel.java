/**
 * 
 */
package net.aocraft.plugins.AOChat;
import java.sql.Timestamp;
import java.util.ArrayList;


/**
 * 
 *
 */
public class ChatChannel {
	// Define fields
	/**
	 * Declares the valid types of channels.
	 */
																
	String chType;											// Type of channel (local or remote)
	String chName ;											// Unique name of channel
	Integer chRange ;										// Range the channel can be heard, 0 for global
	String chPass ;											// Password for channel or null
	String chDescription ;									// Description of channel purpose
	Timestamp chLastLogin ;									// Last Date and time a user logged in
 
	// List of users currently registered to this channel
	ArrayList<String> chUsers;
	
	// Constructors
	public ChatChannel(String pName, String pType) {
		chName = pName;
		chType = pType;
		chRange=0;
		chPass="";
		chDescription=pType.toString().trim() + pName + " chat channel";
		chUsers = new ArrayList<String>();
	}
	public ChatChannel(ChannelConfigurationNode pConfig) {
		
		chType = pConfig.getChannelType();
		chName = pConfig.getChannelName();
		chRange = pConfig.getChannelRange();
		//chPass= pConfig.getChannelPassword();
		chDescription = pConfig.getChannelDescription();
		chUsers = new ArrayList<String>();
		AOChat.getChatInstance().getLogger().info("ChatChannel from Config Node:");
		AOChat.getChatInstance().getLogger().info("NAME ="+chName);
		AOChat.getChatInstance().getLogger().info("DESC ="+chDescription);
		
		
	}
	// Methods
	public ArrayList<String> getChannelUsers() {
		return chUsers;
		
	}
	
	public boolean exists(String pName) {
		return chUsers.contains(pName);
	}
	
	public void addChannelUser(String pName) {				// Adds a user to the channel 
		chUsers.add(pName);
		// TODO: Broadcast message to channel
	}
	
	public void removeChannelUser(String pUserName) {		// Remove user from channel
		chUsers.remove(pUserName);
		// TODO: Broadcast message to channel
	}
	
	

	
	
	// Getters and Setters

	public String getChName() {
		return chName;
	}

	public void setChName(String chName) {
		this.chName = chName;
	}
	public String getChType() {
		return chType;
	}

	public void setChType(String chType) {
		this.chType = chType;
	}

	public Integer getChRange() {
		return chRange;
	}

	public void setChRange(Integer chRange) {
		this.chRange = chRange;
	}

	public String getChPass() {
		return chPass;
	}

	public void setChPass(String chPass) {
		this.chPass = chPass;
	}

	public String getChDescription() {
		return chDescription;
	}

	public void setChDescription(String chDescription) {
		this.chDescription = chDescription;
	}
	
}

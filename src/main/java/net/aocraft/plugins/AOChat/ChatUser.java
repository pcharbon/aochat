/**
 *  Needed to hold ignore list and user status (muted, etc)
 */
package net.aocraft.plugins.AOChat;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.Date;

import net.aocraft.plugins.aosuite.AOSuite;

import org.spout.api.chat.ChatArguments;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerChatEvent;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.entity.*;



/**
 * @author Ducky
 *
 */
public class ChatUser implements Listener {
	// Class fields
	/**
	 * Name of the user.
	 * Only users registered to a channel will see the messages of that channel
	 */
	String chUserName;
	
	/**
	 * Last channel the user sent a message to
	 * This chat channel should be where the next message is sent if not specified otherwise by user 
	 * 
	 * @see ChatChannel
	 */
	String chLastUsed;
	
	/**
	 * Represents the directory where this user files are saved
	 * User configuration information is saved in YAML format files
	 * under that directory
	 * 
	 * @see ChannelConfiguration
	 * @see ViewPortConfiguration
	 */
	File userFolder;
	/**
	 * Represents the user channel configuration
	 * Each user that join the server has a channel preferences YAML
	 * file created automatically for him in the plug-in data folder.
	 * The name of the file follows this pattern: /[PlayerName]/channels.yml
	 */
	ChannelConfiguration channelConfig;
	
	
	/**
	 * Represents each chat window defined by the user 
	 * Each viewPort contains a list of channels views
	 * 
	 * @see viewPort
	 * @see ChannelView
	 */
	ArrayList<viewPort> chViewPorts;
	

	// Class Constructor
	public ChatUser(String pUserName){
		
		chUserName = pUserName;
		chViewPorts = new ArrayList<viewPort>();
		chLastUsed = "general"; 							// Default to general channel
	
		
	}

	// Class Methods
	
		
	
	public void removeViewPort(viewPort pViewPort) {
	   for (int i = 0; i < chViewPorts.size(); i++) {
		 if (chViewPorts.get(i) == pViewPort) {
			chViewPorts.remove(i);
			return;
		}
	   }
	}
	
	public void addViewPort(viewPort pViewPort) {
		chViewPorts.add(pViewPort);
	}
	
    // Event Handlers
	/**
	 * Handles the player chat event and send message to user if he is listening
	 * @param event
	 */

	

	@EventHandler(order = Order.DEFAULT_IGNORE_CANCELLED)

	public void onPlayerChatEvent(PlayerChatEvent event) {
	  ChatArguments msgMessage = event.getMessage();
	  String msgUserName = event.getPlayer().getName();
	  String msgChannelName = AOChat.getUsersManager().getChatUser(msgUserName).getChLastUsed();
	  if (msgChannelName == null) {
		  // TODO Default channel name should be defined as part of the plug-in configuration file
		  msgChannelName = "general";
	  }
	  // check if the user is listening to a specific channel in each ViewPort (chat window)
	    viewPort port;
		ChatArguments msg = new ChatArguments("["+msgChannelName+"] <"+msgUserName+"> "+msgMessage.asString());
		for (int i = 0;i < chViewPorts.size();i++) {
			port = chViewPorts.get(i);
				if (port.getChannelView(msgChannelName) != null) {

			    AOSuite.getUsersManager().getUser(chUserName).getPlayer().sendMessage(msg);

			}

		}

	  event.setCancelled(true);

	  

	}

	

	/**
	 * Register the event listener in the spout engine event manager
	 */

	

	public void registerListener() {

		AOChat.getChatInstance().getEngine().getEventManager().registerEvents(this, AOChat.getChatInstance());

	}




	// Class getters and setters

	public String getChUserName() {
		return chUserName;
	}

	public void setChUserName(String chUserName) {
		this.chUserName = chUserName;
	}


	
	public ArrayList<viewPort> getViewPorts() {
		return chViewPorts;
	}

	public String getChLastUsed() {
		return chLastUsed;
	}

	public void setChLastUsed(String chChannelName) {
		this.chLastUsed = chChannelName;
	}


	public ChannelConfiguration getChannelConfiguration() {
		return this.channelConfig;
	}

	public void setChannelConfiguration(ChannelConfiguration pChannelConfig) {
		this.channelConfig = pChannelConfig;
	}

	public File getUserFolder() {
		return userFolder;
	}

	public void setUserFolder(File pUserFolder) {
		this.userFolder = pUserFolder;
	}


	public void addChannelView(String channelName, ChannelView channelView) {
		// Add the view to all viewPorts until client-side phase
		for (int i = 0;i < chViewPorts.size();i++) {
			 chViewPorts.get(i).setChannelView(channelName, channelView);
		}
		
	}
	
	public void removeChannelView(String channelName) {
		// Remove the view from all viewPorts until client-side phase
		for (int i = 0;i < chViewPorts.size();i++) {
			 chViewPorts.get(i).removeChannelView(channelName);
		}
		
	}

	
	public void loadChannels () {
		// default view port until implemented in user configuration
		viewPort vPort = new viewPort();
		Collection<?> nodes = (Collection<?>) channelConfig.getAll();
		ChannelConfigurationNode node;
		for (Iterator<?> iter = nodes.iterator(); iter.hasNext();) {
			node = (ChannelConfigurationNode) iter.next();
	    	String channelName = node.getChannelName();
    		// default channel view values until implemented in user configuration
    		vPort.setChannelView(channelName, new ChannelView("Verdana","White",false, false));
    	   	AOChat.getChannelsManager().getChannel(channelName).addChannelUser(chUserName);
	    	AOChat.getChatInstance().getLogger().info(chUserName+ " joined "+channelName+".");
		}

		this.addViewPort(vPort);

	}

	public void saveChannels() {
		// TODO Auto-generated method stub
		
	}


	

	
}

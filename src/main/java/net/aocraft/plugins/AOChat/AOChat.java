/**
*
*/
package net.aocraft.plugins.AOChat;

import net.aocraft.plugins.aosuite.AOSuite;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import org.spout.api.Engine;
import org.spout.api.Spout;
import org.spout.api.command.CommandRegistrationsFactory;
import org.spout.api.command.annotated.AnnotatedCommandRegistrationFactory;
import org.spout.api.command.annotated.SimpleAnnotatedCommandExecutorFactory;
import org.spout.api.command.annotated.SimpleInjector;
import org.spout.api.exception.ConfigurationException;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.plugin.PluginManager;



/**
* @author Dukmeister
*
*/

public class AOChat extends CommonPlugin {
	/**
	 * Used to identify this plug-in.
	 * The pluginID should be used to indicate the origin of system messages
	 */
	private static String pluginID ; 
	
	/**
	 * Used to reference the parent plugin AOSuite.
	 * The pluginID should be used to indicate the origin of system messages
	 */
	private static AOSuite AOInstance ; 
	
	/**
	 * Used to signify that debug-mode is on.
	 * This default's to the engine debug-mode but can be overwritten at the plugin level
	 */
	private static boolean debugMode;

	/**
	 * Used to signify that method profiling is on.
	 * This default's to false but can be set in the configuration
	 */
	private static boolean profileMode;

	/**
	 *  Users directory
	 */
	private static File usersFolder;
	

	/**
	 * Used to keep track of the current chat channels and their users.
	 */
	private static ChatChannelManager channelsManager = new ChatChannelManager();
	
	/**
	 * Used to keep track of the current chat preferences per user.
	 */
	private static ChatUserManager usersManager = new ChatUserManager();


	/**
	 * Used to handle chat messages distribution to users.<br>
	 * 
	 */
	private static ChatMessageManager messagesManager = new ChatMessageManager();
	
	// Plug-in instance
	private static AOChat chatInstance;
	
	// Game engine 
	private Engine engine;
	// Global chat configuration
	private ChatConfiguration chatConfig;
	// Default channels configuration
	private ChannelConfiguration channelConfig;
	
	/**
	 * This method saves the current configuration to file and releases 
	 * resources used by the plug-in.
	 * <p>
	 * This method should be called when disabling the plug-in or
	 * shutting down the server. 
	 */
	@Override
	public final void onDisable() {

		// Save global chat configuration 

		// Save default channels
		
		// Release instance from memory
	
		// Log Status
		getLogger().info(" v" + getDescription().getVersion() + " has been disabled.");
	}

	@Override
	public void onEnable() {
		engine = this.getEngine();
		setChatInstance(this);
		pluginID = this.getName();
		debugMode = engine.debugMode();
		profileMode = false ;

		// Check for AO Suite Plug-in and add reference (dependency assumed from plugin.xml array depends:[])
		AOSuite instance;
		// check for the AO Suite plug-ins
		PluginManager manager = Spout.getPluginManager();
		instance = (AOSuite) manager.getPlugin("AOSuite");
		if (instance != null) {
			setAOInstance(instance);
			Spout.getEngine().getLogger().info("[AO Chat] Found the AO Suite Plugin...");
		} else {
			Spout.getEngine().getLogger().info("[AO Chat] Cannot find the AO Suite Plugin. Disabling!");
			return;
		}
		// Initialize users folder 
		setUsersFolder(new File(this.getDataFolder().getPath()+System.getProperty("file.separator")+"users"));
		// Load global Configuration
		chatConfig = new ChatConfiguration(this.getDataFolder());
		try {
			chatConfig.load();
			} catch (ConfigurationException e) {
			getLogger().log(Level.WARNING, "Error loading "+ AOChat.getPluginID()+" configuration: ", e);
		}

		// Load default channels
		channelConfig = new ChannelConfiguration(this.getDataFolder());
		try {
				channelConfig.load();
				Collection<?> nodes = (Collection<?>) channelConfig.getAll();
				for (Iterator<?> iter = nodes.iterator(); iter.hasNext();) {
					ChannelConfigurationNode node = (ChannelConfigurationNode) iter.next();
			    	ChatChannel channel = new ChatChannel(node);
			    	channelsManager.addChatChannel(channel);
			    	this.getLogger().info("Channel "+channel.getChName()+" created.");
				}
			
			} catch (ConfigurationException e) {
			getLogger().log(Level.WARNING, "Error loading "+ AOChat.getPluginID()+" channels configuration: ", e);
		}
		
		

		//Commands
		CommandRegistrationsFactory<Class<?>> commandRegFactory = new AnnotatedCommandRegistrationFactory(new SimpleInjector(this), new SimpleAnnotatedCommandExecutorFactory());
		engine.getRootCommand().addSubCommands(this, AoCommand.class, commandRegFactory);
		if (debugMode) {
			engine.getRootCommand().addSubCommands(this, TestCommands.class, commandRegFactory);
		}	
		
	

		
		
		// Events Listener 
		
		engine.getEventManager().registerEvents(new ChatListener(this), this);
	
		// Chat Server
		

		
		// Log Status

		getLogger().info(" v" + getDescription().getVersion() + " has been enabled.");

	}
	
	
	public static void setAOInstance(AOSuite instance) {
		// TODO Auto-generated method stub
		AOInstance = instance;
	}
	
	public static CommonPlugin getAOInstance() {
		return AOInstance;
	}


	@Override
	public void onLoad() {
//		setChatInstance(this);
//		engine = getEngine();
//		chatConfig = new ChatConfiguration(getDataFolder());
//		setUsersFolder(new File(engine.getDataFolder().getPath()+System.getProperty("file.separator")+"users"));
		getLogger().info("[AO Chat] plugin loaded...");
	}

	public static String getPluginID() {
		return pluginID;
	}

	
	/**
	 * @return the chatInstance
	 */
	public static AOChat getChatInstance() {
		return chatInstance;
	}

	/**
	 * @param chatInstance the instance of the chat module plug-in
	 */
	public static void setChatInstance(AOChat pChatInstance) {
		AOChat.chatInstance = pChatInstance;
	}

	
	/**
	 * @return the channelsManager object for the chat module instance
	 */
	public static ChatChannelManager getChannelsManager() {
		return channelsManager;
	}

	/**
	 * @param pUserManager the userManager for the chat module
	 */
	public static void setUsersManager(ChatUserManager pUserManager) {
		usersManager = pUserManager;
	}

	/**
	 * @return the usersManager object for the chat module instance
	 */
	public static ChatUserManager getUsersManager() {
		return usersManager;
	}

	/**
	 * @param channelManager the channelManager to set
	 */
	public static void setChannelsManager(ChatChannelManager pChannelManager) {
		channelsManager = pChannelManager;
	}

	/**
	 * @return the messageManager
	 */
	public static ChatMessageManager getChatMessageManager() {
		return messagesManager;
	}

	/**
	 * @param channelManager the channelManager to set
	 */
	public static void setMessageManager(ChatMessageManager pMessageManager) {
		messagesManager = pMessageManager;
	}
	

	/**
	 * @return the usersFolder
	 */
	public static File getUsersFolder() {
		return usersFolder;
	}

	/**
	 * @param usersFolder the usersFolder to set
	 */
	public static void setUsersFolder(File usersFolder) {
		AOChat.usersFolder = usersFolder;
	}


	
}
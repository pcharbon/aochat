/**
 * 
 */
package net.aocraft.plugins.AOChat;

import java.util.HashMap;

/**
 * Handles the in-memory list of users connected to the chat.
 * This class will manage the users that connect/disconnect to the chat
 *
 */
public class ChatUserManager {
	// Holds the currently connected users and their preferences
	HashMap<String,ChatUser> chatUsers ;
	
	public ChatUserManager(){
		chatUsers = new HashMap<String,ChatUser>();
	}
	
	public void addChatUser (ChatUser pUser) {
		chatUsers.put(pUser.getChUserName(), pUser);

	}
	
	public void removeChatUser(ChatUser pUser) {
		chatUsers.remove(pUser.getChUserName());
	}

	public ChatUser getChatUser(String pUserName) {
		
		return chatUsers.get(pUserName);
	}
	
	public boolean Exists(String pChannelName) {
		return chatUsers.containsKey(pChannelName);
	}
	
}

package net.aocraft.plugins.AOChat;

import java.util.Map;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.ConfigurationHolder;
import org.spout.api.util.config.ConfigurationHolderConfiguration;
import org.spout.api.util.config.ConfigurationNode;
import org.spout.api.util.config.MapConfiguration;

public class ChannelConfigurationNode extends ConfigurationHolderConfiguration {
	
	
	private ConfigurationHolder TYPE = new ConfigurationHolder("local", "type");						// General chat channel type
	private ConfigurationHolder RANGE = new ConfigurationHolder(0, "range");							// Distance the channel can be heard from -- 0 is global
	private ConfigurationHolder PASSWORD = new ConfigurationHolder("", "password");						// Channel password or empty string for none
	private ConfigurationHolder DESCRIPTION = new ConfigurationHolder("Generic chat channel", "description");
	
	
	private final String name;
	private final ChannelConfiguration parent;

	public ChannelConfigurationNode(ChannelConfiguration parent, String channelname) {
		super(new MapConfiguration(parent.getNode("channels", channelname).getValues()));
		this.parent = parent;
		this.name = channelname;
	}


	@Override
	public void load() throws ConfigurationException {
		this.setConfiguration(new MapConfiguration(this.getParent().getNode("channels", this.getChannelName()).getValues()));
		super.load();
	}

	@Override
	public void save() throws ConfigurationException {
		super.save();
		ConfigurationNode node = this.getParent().getNode("channels", this.getChannelName());
		for (Map.Entry<String, Object> entry : this.getValues().entrySet()) {
			node.getNode(entry.getKey()).setValue(entry.getValue());
		}
	}

	
	/**
	 * Gets the channel name of this channel configuration node
	 * @return the channel name
	 */
	public String getChannelName() {
		return this.name;
	}

	/**
	 * Gets the channel type of this channel configuration node
	 * @return the channel type
	 */
	public String getChannelType() {
		return TYPE.getString("local");			// default to local channel type  	
	}

	/**
	 * Gets the channel range of this channel configuration node
	 * @return the channel range
	 */
	public Integer getChannelRange() {
		return RANGE.getInt(0);					// Default to 0 (unlimited range)
	}

	/**
	 * Gets the channel description of this channel configuration node
	 * @return the channel description
	 */

	public String getChannelDescription() {
		return DESCRIPTION.getString("Generic chat channel");	// Default description
	}

	/**
	 * Gets the channel password of this channel configuration node
	 * @return the channel password
	 */

	public String getChannelPassword() {
		return PASSWORD.getString("");			// Default password to none
	}


	/**
	 * Gets the parent configuration of this channel configuration node
	 * @return the parent configuration
	 */
	public ChannelConfiguration getParent() {
		return this.parent;
	}

	/**
	 * Sets some of the default values for this configuration node
	 * @param type default
	 * @param name default
	 * @return this node
	 */
	
	public void setDefaults() {
		
		setChannelType("local");
		setDescription("Generic chat channel");
		setRange(0);
		setPassword("");
	}
	

	/**
	 * Sets the channel type of this channel configuration node
	 * @param pType Channel type for this channel node
	 */
	public void setChannelType(String pType) {
		
		TYPE.setValue(pType);
	}

	/**
	 * Sets the channel description for this channel configuration node
	 * @param pDescription Channel description for this channel node
	 */
	public void setDescription(String pDescription) {
		
		TYPE.setValue(pDescription);
	}

	/**
	 * Sets the channel broadcast range for this channel configuration node
	 * @param pRange Range of broadcast for this channel node
	 */
	public void setRange(Integer pRange) {
		
		TYPE.setValue(pRange);
	}

	/**
	 * Sets the channel password for this channel configuration node
	 * @param pPassword Password for this channel node
	 */
	public void setPassword(String pPassword) {
		
		TYPE.setValue(pPassword);
	}

	
}

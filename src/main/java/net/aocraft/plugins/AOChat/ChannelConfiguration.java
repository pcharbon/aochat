/**
 * 
 */
package net.aocraft.plugins.AOChat;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 * @author Dukmeister
 *
 */
public class ChannelConfiguration extends  YamlConfiguration  {
	private final Map<String, ChannelConfigurationNode> channelNodes = new HashMap<String, ChannelConfigurationNode>();
	public static ChannelConfigurationNode GENERAL;
	public static ChannelConfigurationNode ADMIN;
	public static ChannelConfigurationNode SYSTEM;
	
	public ChannelConfiguration(File dataFolder) {
		super(new File(dataFolder, "channels.yml"));
		GENERAL = getOrCreate("general");
		ADMIN = getOrCreate("admin");
		SYSTEM = getOrCreate("system");
	/*	GENERAL.setDefaults();
		ADMIN.setDefaults();
		SYSTEM.setDefaults();
	*/
		
	}
	
	public Collection<ChannelConfigurationNode> getAll() {
		return channelNodes.values();
	}

	/**
	 * Gets the channel configuration of a certain channel.<br>
	 * Creates a new one if it doesn't exist
	 * @param channel configuration
	 * @return the channel configuration node
	 */
	public ChannelConfigurationNode getOrCreate(ChatChannel channel) {
		return getOrCreate(channel.getChName());
	}

	/**
	 * Gets the channel configuration of a certain channel<br>
	 * Creates a new one if it doesn't exist
	 * @param name of channel in the configuration
	 * @return the channel configuration node
	 */
	public ChannelConfigurationNode getOrCreate(String channelname) {
		synchronized (channelNodes) {
			ChannelConfigurationNode node = channelNodes.get(channelname);
			if (node == null) {
				node = new ChannelConfigurationNode(this, channelname);
				channelNodes.put(channelname, node);
			}
			return node;
		}
	}
		
	@Override
	public void load() throws ConfigurationException {
		super.load();
		for (ChannelConfigurationNode node : getAll()) {
			node.load();
		}
	}

	@Override
	public void save() throws ConfigurationException {
		for (ChannelConfigurationNode node : getAll()) {
			node.save();
		}
		super.save();
	}
}

package net.aocraft.plugins.AOChat;
/**
 * This class will handle all /command functionality for the plug-in and sub-modules
 * <p>
 * The AnnotatedCommand class extensions should be registered with the CommandFactory but should only be
 * used for forwarding calls to the functionality provided by the CommandManager. Multiple commands at 
 * different levels of the command structure can thus call the same functionality with different names.
 *   
 * @author Ducky
 *
 */
public class CommandManager {

}
